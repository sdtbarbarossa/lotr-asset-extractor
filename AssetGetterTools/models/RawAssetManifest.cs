﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssetGetterTools.models
{
    [global::ProtoBuf.ProtoContract()]
    public partial class RawAssetManifest : global::ProtoBuf.IExtensible
    {
        private global::ProtoBuf.IExtension __pbn__extensionData;
        global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
            => global::ProtoBuf.Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [global::ProtoBuf.ProtoMember(1)]
        public int Version { get; set; }

        [global::ProtoBuf.ProtoMember(3)]
        [global::System.ComponentModel.DefaultValue(null)]
        public string Platform { get; set; }

        [global::ProtoBuf.ProtoMember(5)]
        [global::System.ComponentModel.DefaultValue(null)]
        public string Environment { get; set; }

        [global::ProtoBuf.ProtoMember(6)]
        public ulong Timestamp { get; set; }

        [global::ProtoBuf.ProtoMember(7)]
        public ulong Revision { get; set; }

        [global::ProtoBuf.ProtoMember(2)]
        public global::System.Collections.Generic.List<RawAssetManifestRecord> Records { get; } = new global::System.Collections.Generic.List<RawAssetManifestRecord>();

    }

    [global::ProtoBuf.ProtoContract()]
    public partial class RawAssetManifestRecord : global::ProtoBuf.IExtensible
    {
        private global::ProtoBuf.IExtension __pbn__extensionData;
        global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
            => global::ProtoBuf.Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [global::ProtoBuf.ProtoMember(1)]
        [global::System.ComponentModel.DefaultValue(null)]
        public string Name { get; set; }

        [global::ProtoBuf.ProtoMember(3)]
        public int Size { get; set; }

        [global::ProtoBuf.ProtoMember(4)]
        public bool Shared { get; set; }

        [global::ProtoBuf.ProtoMember(5)]
        public int Rank { get; set; }

        [global::ProtoBuf.ProtoMember(6)]
        public int PackageType { get; set; }

        [global::ProtoBuf.ProtoMember(9)]
        [global::System.ComponentModel.DefaultValue(null)]
        public string Hash { get; set; }

        [global::ProtoBuf.ProtoMember(7)]
        public global::System.Collections.Generic.List<RawAssetManifestRecordEntry> Entries { get; } = new global::System.Collections.Generic.List<RawAssetManifestRecordEntry>();

        [global::ProtoBuf.ProtoMember(8)]
        public global::System.Collections.Generic.List<string> Dependencies { get; } = new global::System.Collections.Generic.List<string>();

    }

    [global::ProtoBuf.ProtoContract()]
    public partial class RawAssetManifestRecordEntry : global::ProtoBuf.IExtensible
    {
        private global::ProtoBuf.IExtension __pbn__extensionData;
        global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
            => global::ProtoBuf.Extensible.GetExtensionObject(ref __pbn__extensionData, createIfMissing);

        [global::ProtoBuf.ProtoMember(1)]
        [global::System.ComponentModel.DefaultValue(null)]
        public string AssetName { get; set; }

    }
}
